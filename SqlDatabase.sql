USE [INFOCLIENTES]
GO
/****** Object:  Table [dbo].[Clientes]    Script Date: 31/07/2018 9:30:01 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Clientes](
	[ClNIt] [varchar](900) NOT NULL,
	[ClNombresApe] [varchar](300) NOT NULL,
	[ClDireccion] [varchar](500) NOT NULL,
	[ClTel] [varchar](15) NOT NULL,
	[MnIdMunicipio] [varchar](6) NOT NULL,
	[ClCupo] [numeric](18, 4) NOT NULL,
	[ClSaldoCupo] [numeric](18, 4) NULL,
	[ClPorceVisitas] [numeric](5, 2) NULL,
 CONSTRAINT [PK_Clientes] PRIMARY KEY CLUSTERED 
(
	[ClNIt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CuposCliente]    Script Date: 31/07/2018 9:30:01 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CuposCliente](
	[CcSec] [int] IDENTITY(1,1) NOT NULL,
	[CcClNit] [varchar](900) NOT NULL,
	[CcValor] [numeric](18, 4) NOT NULL,
	[CcFecha] [datetime] NOT NULL,
 CONSTRAINT [PK_CuposCliente] PRIMARY KEY CLUSTERED 
(
	[CcSec] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Departamento]    Script Date: 31/07/2018 9:30:01 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Departamento](
	[DpPais] [varchar](3) NOT NULL,
	[DpCodDpto] [varchar](3) NOT NULL,
	[DpNomDpto] [varchar](100) NOT NULL,
 CONSTRAINT [PK_Departamento] PRIMARY KEY CLUSTERED 
(
	[DpCodDpto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Municipio]    Script Date: 31/07/2018 9:30:01 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Municipio](
	[MnDepartamento] [varchar](3) NOT NULL,
	[MnIdMunicipio] [varchar](6) NOT NULL,
	[MnCodMunicipio] [varchar](3) NOT NULL,
	[MnNombreMunicipio] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Municipio] PRIMARY KEY CLUSTERED 
(
	[MnIdMunicipio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Pais]    Script Date: 31/07/2018 9:30:01 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pais](
	[PsCodPais] [varchar](3) NOT NULL,
	[PsNomPais] [varchar](100) NOT NULL,
	[PsCodIso] [varchar](3) NOT NULL,
 CONSTRAINT [PK_Pais] PRIMARY KEY CLUSTERED 
(
	[PsCodPais] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Vendedor]    Script Date: 31/07/2018 9:30:01 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Vendedor](
	[VdDocumento] [bigint] NOT NULL,
	[VdNombresApe] [varchar](200) NOT NULL,
	[VdFechaCrea] [datetime] NOT NULL,
	[VdDireccion] [varchar](500) NOT NULL,
	[VdTelefono] [varchar](20) NOT NULL,
 CONSTRAINT [PK_Vendedor] PRIMARY KEY CLUSTERED 
(
	[VdDocumento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Visitas]    Script Date: 31/07/2018 9:30:01 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Visitas](
	[VsSec] [int] IDENTITY(1,1) NOT NULL,
	[VsFeha] [datetime] NOT NULL,
	[VsValorNeto] [decimal](18, 4) NOT NULL,
	[VsValorVisita] [decimal](18, 4) NOT NULL,
	[VsObservacion] [varchar](max) NOT NULL,
	[ClNit] [varchar](900) NOT NULL,
	[VdDocumento] [bigint] NOT NULL,
	[VsIdMuni] [varchar](6) NULL,
 CONSTRAINT [PK_Visitas] PRIMARY KEY CLUSTERED 
(
	[VsSec] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[CuposCliente]  WITH CHECK ADD  CONSTRAINT [FK_CuposCliente_Clientes] FOREIGN KEY([CcClNit])
REFERENCES [dbo].[Clientes] ([ClNIt])
GO
ALTER TABLE [dbo].[CuposCliente] CHECK CONSTRAINT [FK_CuposCliente_Clientes]
GO
ALTER TABLE [dbo].[Visitas]  WITH CHECK ADD  CONSTRAINT [FK_Visitas_Clientes] FOREIGN KEY([ClNit])
REFERENCES [dbo].[Clientes] ([ClNIt])
GO
ALTER TABLE [dbo].[Visitas] CHECK CONSTRAINT [FK_Visitas_Clientes]
GO
ALTER TABLE [dbo].[Visitas]  WITH CHECK ADD  CONSTRAINT [FK_Visitas_Vendedor] FOREIGN KEY([VdDocumento])
REFERENCES [dbo].[Vendedor] ([VdDocumento])
GO
ALTER TABLE [dbo].[Visitas] CHECK CONSTRAINT [FK_Visitas_Vendedor]
GO
